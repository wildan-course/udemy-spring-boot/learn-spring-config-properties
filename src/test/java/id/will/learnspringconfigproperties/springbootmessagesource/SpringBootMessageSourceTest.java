package id.will.learnspringconfigproperties.springbootmessagesource;

import java.util.Locale;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Component;

import lombok.Setter;

@SpringBootTest(classes=SpringBootMessageSourceTest.TestApplication.class)
public class SpringBootMessageSourceTest {
	
	@Autowired
	private TestApplication.SampleSource sampleSource;
	
	@Test
	void helloWill( ) {
		
		
		Assertions.assertEquals("Hello Will", sampleSource.helloWill(Locale.ENGLISH));
		Assertions.assertEquals("Halo Will", sampleSource.helloWill(new Locale("in", "id")));
	}
	
	

	@SpringBootApplication
	public static class TestApplication{
		
		
		@Component
		public static class SampleSource implements MessageSourceAware {
		
			@Setter
			private MessageSource messageSource;
			
			public String helloWill(Locale locale) {
				return messageSource.getMessage("hello", new Object[] {"Will"}, locale);
			}
			
		}
		
	}
	
}
