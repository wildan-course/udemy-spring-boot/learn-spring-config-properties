package id.will.learnspringconfigproperties.environment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@SpringBootTest
public class EnvironmentTest {

	@Autowired
	private Environment environment;
	
	@Test
	void testEnvironment() {
		String javaHome = environment.getProperty("JAVA_HOME");
//		Assertions.assertEquals("C:\\Program Files\\Amazon Corretto\\jdk11.0.15_9", javaHome);
	}
	
}
