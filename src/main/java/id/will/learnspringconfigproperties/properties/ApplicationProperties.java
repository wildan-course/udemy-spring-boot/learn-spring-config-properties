package id.will.learnspringconfigproperties.properties;

import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ConfigurationProperties("application")
public class ApplicationProperties {
	
	private Date expiredDate;
	
	private Duration defaultTimeout;

	private String name;
	private Integer version;
	private boolean productionMode;
	
	private DatabaseProperties database;
	
	private List<Role> defaultRoles;
	
	private Map<String, Role> roles;
	
	@Setter
	@Getter
	public static class DatabaseProperties {
		
		private String username;
		private String password;
		private String database;
		private String url;
		
		private List<String> whitelist;
		
		private Map<String, Integer> maxTablesSize;
		
	}
	
	@Setter
	@Getter
	public static class Role {
		
		private String id;
		private String name;
		
	}
	
}
