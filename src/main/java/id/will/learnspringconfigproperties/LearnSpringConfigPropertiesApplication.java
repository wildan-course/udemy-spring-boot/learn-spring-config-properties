package id.will.learnspringconfigproperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.convert.ApplicationConversionService;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.ConversionService;

import id.will.learnspringconfigproperties.converter.StringToDateConverter;
import id.will.learnspringconfigproperties.properties.ApplicationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
	ApplicationProperties.class
})

public class LearnSpringConfigPropertiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnSpringConfigPropertiesApplication.class, args);
	}
	
	@Bean
	public ConversionService conversionService(StringToDateConverter stringToDateConverter) {
		ApplicationConversionService conversionService = new ApplicationConversionService();
		conversionService.addConverter(stringToDateConverter);
		return conversionService;
	}

}
