package id.will.learnspringconfigproperties.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import id.will.learnspringconfigproperties.properties.ApplicationProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@AllArgsConstructor
@Slf4j
public class ApplicationPropertiesRunner implements ApplicationRunner{

	private ApplicationProperties applicationProperties;
	
	public void run(ApplicationArguments args) throws Exception {
		log.info(applicationProperties.getName());
		log.info(String.valueOf(applicationProperties.getVersion()));
		log.info(String.valueOf(applicationProperties.isProductionMode()));
	}

	
}
